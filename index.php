<?php get_header(); ?>
<div class="section">
    <div class="container" role="main">
        <div class="row">
            <div class="col-md-8">
                <?php if(have_posts()): ?>
                    <?php while(have_posts()) : ?>
                        <?php the_post();?>
                        <?php get_template_part('content', get_post_format()); ?>
                    <?php endwhile;?>
                <?php endif;?>
            </div>
            <div class="col-md-4">
                <?php get_sidebar()?>
            </div>            
        </div>
    </div>
</div>
<?php get_footer();?>