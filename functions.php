<?php

function temaku_enqueue(){
    wp_enqueue_script('bootstrap',
    get_template_directory_uri().'/js/bootstrap.min.js',
            array('jquery'));
}
add_action('wp_footer', 'temaku_enqueue');

function temaku_setup(){
    register_nav_menu('primary', __('Navigation Menu', 'temaku'));
}
add_action('after_setup_theme', 'temaku_setup');

function temaku_widget_init(){
    register_sidebar(array(
        'name'          => __('Area Widget Utama', 'temaku'),
        'id'            => 'kanan',
        'description'   => __('Tampil pada bagian kana website', 'temaku'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>'
    ));
}
add_action('widgets_init', 'temaku_widget_init');